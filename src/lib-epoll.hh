#ifndef _MYINCLUDE_LIB_EPOLL_HH_
#define _MYINCLUDE_LIB_EPOLL_HH_

namespace epoll
{

class manager;

template < typename io_object > class set
{
public:
    static set& instance();
private:
    set() { }
    ~set() { }
};

template < typename io_object >
set< io_object >& set< io_object >::instance()
{
    static set singelton;
    return singelton;
}

} // epoll

#endif//_MYINCLUDE_LIB_EPOLL_HH_
